---
- name: Install necessities and nice-to-haves
  apt:
    name:
      - apt-transport-https
      - ca-certificates
      - cifs-utils
      - curl
      - software-properties-common
      - gnupg-agent
      - git
      - htop
      - iftop
      - iotop
      - mosh
      - nfs-common
      - screen
      - sudo
      - unattended-upgrades
      - vim
      - zsh
      - whois
      - python3
      - python3-pip
      - python3-virtualenv
      - python3-passlib
      - python3-openssl
  tags:
    - dependencies

- name: Ensure Docker is not installed via Snap
  snap:
    name: docker
    state: absent
  when:
    - ansible_facts['distribution'] == 'Ubuntu'
    - ansible_facts['distribution_major_version'] is version('20.04', '>=')

- name: Remove older docker.io based packages
  apt:
    name:
      - docker
      - docker.io
      - containerd
      - runc
      - docker-engine
    state: absent

- name: Add an apt key for Docker-ce Repo
  apt_key:
    url: https://download.docker.com/linux/ubuntu/gpg
    state: present

- name: Add the Docker-ce Repo
  apt_repository:
    repo: deb https://download.docker.com/linux/ubuntu {{ubuntu_release}} stable
    state: present

- name: Install Docker and docker-compose
  apt:
    name:
      - docker-ce
      - docker-ce-cli
      - docker-compose
      - containerd.io
  tags:
    - dependencies

- name: Correct Python version selected
  alternatives:
    name: python
    link: /usr/bin/python
    path: /usr/bin/python3

- name: Install Passlib
  pip:
    name: passlib
    executable: pip3

- name: timezone - configure /etc/timezone
  copy:
    content: "{{ common_timezone | regex_replace('$', '\n') }}"
    dest: /etc/timezone
    owner: root
    group: root
    mode: 0644
  register: common_timezone_config

- name: timezone - Set localtime to UTC
  file: src=/usr/share/zoneinfo/Etc/UTC dest=/etc/localtime
  when: common_timezone_config.changed

- name: timezone - reconfigure tzdata
  command: dpkg-reconfigure --frontend noninteractive tzdata
  when: common_timezone_config.changed

- name: Ensure locale en_US.UTF-8 locale is present
  locale_gen:
    name: en_US.UTF-8
    state: present

- name: adding existing user '{{ homelab_ssh_user }}' to group docker
  user:
    name: "{{ homelab_ssh_user }}"
    groups: docker
    append: true
  become: true
